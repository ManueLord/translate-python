from deep_translator import GoogleTranslator
import json
text = 'happy'
path =  'your_file.txt'

# Class for create translate
class translate:
    # Translate word on only one language
    def word(word, lan):
        translate = GoogleTranslator(source='auto', target=lan).translate(text=word)
        return translate
    # Translate file on only one language
    def onFile(fil, lan):
        translate = GoogleTranslator(source='en', target=lan).translate_file(fil)
        return translate
    # Translate word for all languages
    def wordLoop(word):
        tranlates = []
        # tranlates = []
        langs_dict = GoogleTranslator().get_supported_languages(as_dict=True)
        for n, d in langs_dict.items():
            t = GoogleTranslator(source='auto', target=d).translate(text=word)
            tranlates.append(t)
        return tranlates
    # Translate file for all languages
    def flieLoop(fil):
        tranlates = []
        langs_dict = GoogleTranslator().get_supported_languages(as_dict=True)
        for n, d in langs_dict.items():
            t = GoogleTranslator(source='en', target=d).translate_file(fil)
            tranlates.append({t})
            # dictT = '{' + json.JSONEncoder().encode(t) + '}'
            # result = eval(dictT)
            # tranlates.append(result)
        return tranlates
  
# Init 
# if __name__=="__main__":
#     translate().word('happy')
#     translate().flie('your_file.txt')
#     translate().wordLoop('happy')
#     translate().flieLoop('your_file.txt')