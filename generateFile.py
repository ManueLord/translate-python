import os
import json
from translate import translate
from deep_translator import GoogleTranslator

word = 'Test'
key = 'shared{0}'.format(word)

langs_dict = GoogleTranslator().get_supported_languages(as_dict=True)

class generateFile:
    def create(self, option, lan):
        print(option, lan)
    def update(self, option, lan):
        print(option, lan)
    def read(self):
        print(3)
    def delete(self):
        print(4)

if __name__=="__main__":
    text0 = "Don\'t option valid"
    text1 = "       Select option for translate:"
    text2 = "1.- More Words\n2.- Files"
    text3 = "--------------------------------------------"
    text4 = "            How many language?"
    text5 = "1.- ALL\n2.-Select language"
    text6 = "Select language for translate"
    text7 = "| af.- afrikaans                   | sq.- albanian   | am.- amharic        | ar.- arabic     | hy.- armenian  |\n"
    text8 = "| as.- assamese                    | ay.- aymara     | az.- azerbaijani    | bm.- bambara    | eu.- basque    |\n"
    text9 = "| be.- belarusian                  | bn.- bengali    | bho.- bhojpuri      | bs.- bosnian    | bg.- bulgarian |\n"
    text10 = "| ca.- catalan                     | ceb.- cebuano   | ny.- chichewa       | zh-CN.- chinese (simplified)     |\n"
    text11 = "| zh-TW.- chinese (traditional)    | co.- corsican   | hr.- croatian       | cs.- czech      | da.- danish    |\n"
    text12 = "| dv.- dhivehi                     | doi.- dogri     | nl.- dutch          | en.- english    | eo.- esperanto |\n"
    text13 = "| et.- estonian                    | ee.- ewe        | tl.- filipino       | fi.- finnish    | fr.- french    |\n"
    text14 = "| fy.- frisian                     | gl.- galician   | ka.- georgian       | de.- german     | el.- greek     |\n"
    text15 = "| gn.- guarani                     | gu.- gujarati   | ht.- haitian creole | ha.- hausa      | haw.- hawaiian |\n"
    text16 = "| iw.- hebrew                      | hi.- hindi      | hmn.- hmong         | hu.- hungarian  | is.- icelandic |\n"
    text17 = "| ig.- igbo                        | ilo.- ilocano   | id.- indonesian     | ga.- irish      | it.- italian   |\n"
    text18 = "| ja.- japanese                    | jw.- javanese   | kn.- kannada        | kk.- kazakh     | km.- khmer     |\n"
    text19 = "| rw.- kinyarwanda                 | gom.- konkani   | ko.- korean         | ku.- kurdish (kurmanji)          |\n"
    text20 = "| kri.- krio                       | ckb.- kurdish (sorani)                | ky.- kyrgyz     | lo.- lao       |"
    text21 = " | la.- latin                       | lv.- latvian    | ln.- lingala        | lt.- lithuanian | lg.- luganda   |\n"
    text22 = "| lb.- luxembourgish               | mk.- macedonian | mai.- maithili      | mg.- malagasy   | ms.- malay     |\n"
    text23 = "| ml.- malayalam                   | mt.- maltese    | mi.- maori          | mr.- marathi    | lus.- mizo     |\n"
    text24 = "| mni- Mtei.- meiteilon (manipuri) | mn.- mongolian  | my.- myanmar        | ne.- nepali     | no.- norwegian |\n"
    text25 = "| or.- odia (oriya)                | om.- oromo      | ps.- pashto         | fa.- persian    | pl.- polish    |\n"
    text26 = "| pt.- portuguese                  | pa.- punjabi    | qu.- quechua        | ro.- romanian   | ru.- russian   |\n"
    text27 = "| sm.- samoan                      | sa.- sanskrit   | gd.- scots gaelic   | nso.- sepedi    | sr.- serbian   |\n"
    text28 = "| st.- sesotho                     | sn.- shona      | sd.- sindhi         | si.- sinhala    | sk.- slovak    |\n"
    text29 = "| sl.- slovenian                   | so.- somali     | es.- spanish        | su.- sundanese  | sw.- swahili   |\n"
    text30 = "| sv.- swedish                     | tg.- tajik      | ta.- tamil          | tt.- tatar      | te.- telugu    |\n"
    text31 = "| th.- thai                        | ti.- tigrinya   | ts.- tsonga         | tr.- turkish    | tk.- turkmen   |\n"
    text32 = "| ak.- twi                         | uk.- ukrainian  | ur.- urdu           | ug.- uyghur     | uz.- uzbek     |\n"
    text33 = "| vi.- vietnamese                  | cy.- welsh      | xh.- xhosa          | yi.- yiddish    | yo.- yoruba    |\n"
    text34 = "| zu.- zulu                        +-----------------+---------------------+-----------------+----------------+\n"
    text35 = "+----------------------------------+-----------------+---------------------+-----------------+----------------+\n"
    text36 = "divide the languages with \',\'"
    # text = ''
    # print(len(langs_dict))
    print("     Select one option for have file:")
    print("1.- Create\n2.- Update\n3.- Read\n4.- Delete")
    print(text3)
    option = input('Option: ')
    if option == '1':
        print(f"{text1}\n{text2}\n{text3}")
        selectTranslate = input('Select: ')
        if selectTranslate == '1':
            print(f"{text4}\n{text5}\n{text3}")
            result = input('The result is: ')
            if result == '1':
                language = langs_dict
            elif result == '2':
                print('',text35,text7,text8,text9,text10,text11,text12,text13,text14,text15,text16,text17,text18,text19,text20)
                print(text21,text22,text23,text24,text25,text26,text27,text28,text29,text30,text31,text32,text33,text34,text35)
                print(text36)
                witeLanguage = input('Write the languages for translate: ')
                witeLanguage = witeLanguage.replace(' ', '').split(',')
                language = {}
                for x in witeLanguage:
                    key = list(langs_dict.keys())[list(langs_dict.values()).index(x)]
                    print( key)
                    language[key] = x
                    # print ((list(langs_dict.keys())[list(langs_dict.values()).index(x)]))
                    # key = (list(langs_dict.keys())[list(langs_dict.values()).index(x)])
                    # print key
            generateFile().create('MoreFiles', language)
        elif selectTranslate == '2':
            print('',text35,text7,text8,text9,text10,text11,text12,text13,text14,text15,text16,text17,text18,text19,text20)
            print(text21,text22,text23,text24,text25,text26,text27,text28,text29,text30,text31,text32,text33,text34,text35)
            language = input('Write only one language for translate: ')
            generateFile().create('Translate', language)
        else:
            print(text0)
    elif option == '2':
        print(f"{text1}\n{text2}\n{text3}\n")
        typeTranslate = input('Select: ')
        if int(typeTranslate) == 1:
            print('     Type option for charge words: ')
            print('1.- Write Words\n2.- Files')
            print(text3)
            typeTranslate = input('Type: ')
            if typeTranslate == '1':
                print(f"{text4}\n{text5}\n{text3}")
                result = input('The result is: ')
                if result == '1':
                    language = langs_dict
                elif result == '2':
                    print('',text35,text7,text8,text9,text10,text11,text12,text13,text14,text15,text16,text17,text18,text19,text20)
                    print(text21,text22,text23,text24,text25,text26,text27,text28,text29,text30,text31,text32,text33,text34,text35)
                    print(text36)
                    witeLanguage = input('Write the languages for translate: ')
                    witeLanguage = witeLanguage.replace(' ', '').split(',')
                    language = {}
                    for x in witeLanguage:
                        key = list(langs_dict.keys())[list(langs_dict.values()).index(x)]
                        print( key)
                        language[key] = x
                generateFile().update('tranlateMoreWord', language)
            elif typeTranslate == '2':
                print(f"{text4}\n{text5}\n{text3}")
                result = input('The result is: ')
                if result == '1':
                    language = langs_dict
                elif result == '2':
                    print('',text35,text7,text8,text9,text10,text11,text12,text13,text14,text15,text16,text17,text18,text19,text20)
                    print(text21,text22,text23,text24,text25,text26,text27,text28,text29,text30,text31,text32,text33,text34,text35)
                    print(text36)
                    witeLanguage = input('Write the languages for translate: ') 
                    witeLanguage = witeLanguage.replace(' ', '').split(',') 
                    language = {}
                    for x in witeLanguage:
                        key = list(langs_dict.keys())[list(langs_dict.values()).index(x)]
                        print( key)
                        language[key] = x
                generateFile().update('translateFiles', language)
            else:
                print(text0)
        elif typeTranslate == '2':
            print(f"{text4}\n{text5}\n{text3}")
            result = input('The result is: ')
            if result == '1':
                language = langs_dict
            elif result == '2':
                print('',text35,text7,text8,text9,text10,text11,text12,text13,text14,text15,text16,text17,text18,text19,text20)
                print(text21,text22,text23,text24,text25,text26,text27,text28,text29,text30,text31,text32,text33,text34,text35)
                language = input('Write only one language for translate: ')
            generateFile().update('translateFile', language)
        else:
            print(text0)
    elif option == '3':
        generateFile().read()
    elif option == '4':
        generateFile().delete()
    else:
        print(text0)
